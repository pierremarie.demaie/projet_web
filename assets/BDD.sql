-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : 05/11/2020 à 11:17
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.6-1+lenny4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `BDD`
--

-- --------------------------------------------------------

--
-- Structure de la table `objets`
--

CREATE TABLE IF NOT EXISTS `tbl_objets` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) collate utf8_unicode_ci NOT NULL,
  `type` varchar(20) collate utf8_unicode_ci NOT NULL,
  `indice` varchar(20) collate utf8_unicode_ci NOT NULL,
  `lat` double,
  `long` double,
  `zoom` int(11),
  `bloque` int(11),
  `libere` int(11),
  `icone` varchar(20) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `objets`
--

INSERT INTO `tbl_objets`(`id`, `nom`, `type`,`indice`, `lat`, `long`, `zoom`, `bloque`, `libere`, `icone`) VALUES
(1, 'Paul', 'objet',NULL, 0, 0, 13, 2, NULL, NULL),
(2, 'Carte', 'objet',NULL, 1, 1, 13, 5, 1, NULL),
(3, 'Clef', 'objet',NULL,2, 2,13, 5, 2, NULL),
(4, 'Clef', 'objet',NULL, 3, 3,13, 5, 2, NULL),
(5, 'Coffre', 'objet',NULL,4,4,13,NULL,NULL,NULL)
;

-- --------------------------------------------------------




